import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*;
import java.util.*;
import java.io.*;
public class client extends JFrame
{	
	JPanel p=new JPanel();
	Scanner sc;
	PrintWriter pw;
	JLabel l1,l2;
	JButton b1,b2;
	JButton b[] =new JButton[9];
	int selected[]=new int[9];
	int lastwin;
	public client()
	{
		super("client");
		try{
	
			for(int i=0;i<9;i++)
			{
				selected[i]=-1;
			}
			p.setLayout(new GridLayout(3,3));
			
			Socket s=new Socket("localhost",7777);
			pw=new PrintWriter(s.getOutputStream(),true);
			sc=new Scanner(s.getInputStream());
			for(int i=0;i<9;i++)
			{
				b[i]=new JButton();
				b[i].setText("");
				p.add(b[i]);
				b[i].addActionListener(new button());
				
			}	
				
			setSize(500,500);
			this.getContentPane().add(p);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setLocation(620,20);
			setVisible(true);
			for(int i=0;i<9;i++)
					b[i].setEnabled(false);
			while(true)
			{
				checkmove();
				int a=sc.nextInt();	
				if(a==15)
					req();				
				else if(a==20)				
					System.exit(0);				
				else if(a==100)
					next(-1);	
				else
				{	
					b[a].setText("O");
				
					selected[a]=0;		
					for(int i=0;i<9;i++)
						if(selected[i]==-1)
						b[i].setEnabled(true);
				}	
			}
		}
		
		catch(Exception e)
		{System.out.println(e);}
	}	
	public class button implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			JButton bt=(JButton)e.getSource();
			for(int i=0;i<9;i++)
				b[i].setEnabled(false);
			
			if(bt==b1)
			{
				pw.println(15);
				playagain();	
			}
			else if(bt==b2)
			{	
				pw.println(20);
				System.exit(0);
			}
			bt.setText("X");
			if(bt==b[0])
			{pw.println(0);selected[0]=0;}
			
			else if(bt==b[1])
			{pw.println(1);selected[1]=0;}
			else if(bt==b[2])
			{pw.println(2);selected[2]=0;}
			else if(bt==b[3])
			{pw.println(3);selected[3]=0;}
			else if(bt==b[4])
			{pw.println(4);selected[4]=0;}
			else if(bt==b[5])
			{pw.println(5);selected[5]=0;}
			else if(bt==b[6])
			{pw.println(6);selected[6]=0;}
			else if(bt==b[7])
			{pw.println(7);selected[7]=0;}
			else if(bt==b[8])
			{pw.println(8);selected[8]=0;}
			if(check() == 1)
				next(1);	
			checkmove();
		}
	}
	public int check()
	{	
		if(b[0].getText().equals(b[1].getText()) && b[1].getText().equals(b[2].getText()) && b[0].getText()!="")
		{return 1;}
		else if(b[3].getText().equals(b[4].getText()) && b[4].getText().equals(b[5].getText()) && b[3].getText()!="")
		return 1;
		else if(b[6].getText().equals(b[7].getText()) && b[7].getText().equals(b[8].getText()) && b[6].getText()!="")
		return 1;
		else if(b[0].getText().equals(b[3].getText()) && b[3].getText().equals(b[6].getText()) && b[0].getText()!="")
		return 1;			
		else if(b[1].getText().equals(b[4].getText()) && b[4].getText().equals(b[7].getText()) && b[1].getText()!="")
		return 1;			
		else if(b[2].getText().equals(b[5].getText()) && b[5].getText().equals(b[8].getText()) && b[2].getText()!="")
		return 1;				
		else if(b[0].getText().equals(b[4].getText()) && b[4].getText().equals(b[8].getText()) && b[0].getText()!="")
		return 1;				
		else if(b[2].getText().equals(b[4].getText()) && b[4].getText().equals(b[6].getText()) && b[2].getText()!="")
		return 1;
		else
			return 0; 
	}
	public void checkmove()
	{	int f=0;
		for(int i=0;i<9;i++)
			if(selected[i]==-1)
				f=1;
		if(f==0)
		{	next(0);
			reset();
		}
			
	}
	public void next(int a)
	{		
		l1 =  new JLabel("");;
		getContentPane().removeAll();
		l1.setBounds(200,100,100,50);
		add(l1);
		setLayout(null);
		if(a==1){
			l1.setText("You win...");
			lastwin=1;
			pw.println(100);	
		}
		else if(a==0)
			l1.setText("Tie...");
		
		else if(a==-1){	
			l1.setText("You Lose..");
			lastwin=0;
		}	
			repaint();
	}
	public void playagain(){	
		getContentPane().removeAll();
		setLayout(new BorderLayout());
		p.setVisible(true);
		add(p);
		reset();
		repaint();
		
	}
	public void reset()
	{	
		if(lastwin==1){
			for(int i=0;i<9;i++)
			{	
				b[i].setText("");
				b[i].setEnabled(true);
				selected[i]=-1;
			}
		}
		else{	
			for(int i=0;i<9;i++)
			{	
				b[i].setText("");
				b[i].setEnabled(false);
				selected[i]=-1;
			}
			
		}
	}
	
	public void req()
	{	
		
		l1.setText("Want to play again");
		l1.setBounds(200,100,200,50);
		b1=new JButton("yes");
		b1.setVisible(true);
		b1.setBounds(200,200,70,30);
		b1.addActionListener(new button());
		add(b1);
		b2=new JButton("no");
		b2.setBounds(320,200,70,30);
		b2.addActionListener(new button());
		add(b2);
		repaint();
	}
	public static void main(String st[]) throws IOException
	{
		new client();
		
	}
	
}
